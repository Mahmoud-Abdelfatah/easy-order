<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('customer.orders');
});
Route::get('/orders',function(){
 return view('customer.orders');
})->name('orders');
Route::get('/orders/table',function(){
    return view('customer.table.index');
})->name('table');
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
