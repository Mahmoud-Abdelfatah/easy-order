@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row row-cols-1 row-cols-md-3 g-4">
        <div class="col">
          <div class="card h-100">
            <img src="{{asset('img/customer/table.jfif')}}" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">Table Order</h5>
              <p class="card-text">You can Order your meal and it will serve to your table in no time.</p>
              <a href="{{route('table')}}" class="btn btn-primary">Order Now</a>
            </div>

          </div>
        </div>
        <div class="col">
          <div class="card h-100">
            <img src="{{asset('img/customer/delivery.jpg')}}" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">Delivery Order</h5>
              <p class="card-text">when can reach you where ever you are .</p>
              <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#branchModal">Order Now</a>
            </div>

          </div>
        </div>
        <div class="col">
          <div class="card h-100">
            <img src="{{asset('img/customer/pickup.jpg')}}" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">Pickup Order</h5>
              <p class="card-text">You can tell us your order and will make it ready for you.</p>
              <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#branchModal">Order Now</a>
            </div>

          </div>
        </div>
      </div>  

</div>


  <!-- Modal -->
  <div class="modal fade" id="branchModal" tabindex="-1" role="dialog" aria-labelledby="branchModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="branchModalLabel">Select Your Branch</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <select name="branch" id="" class="form-control">
              <option value="">-- Select --</option>
              <option value="b1" >Cairo Branch</option>
          </select>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">submit</button>
        </div>
      </div>
    </div>
  </div>

@endsection
